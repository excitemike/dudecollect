export const ARM_THICKNESS:number = 5;
export const GRAVITY_FOR_JUST_PLAYER:number = 60;
export const GRAVITY_PER_FRIEND:number = 9;
export const PLAYER_START_POSITION:Phaser.Geom.Point = new Phaser.Geom.Point(175, 250);
export const PLATFORM_POSITION:Phaser.Geom.Point = new Phaser.Geom.Point(175, 275);;
export const NUM_FRIENDS:number = 10;
export const LAST_LEVEL:number = 10;
export const INITIAL_SPAWN_AREA_BOTTOM:number = 250;
export const INITIAL_SPAWN_AREA_TOP:number = 0;
export const SPAWN_AREA_TOP_INCREASE_PER_LEVEL:number = -42.5;
export const MAX_ARM_LENGTH:number = 40;
export const ARM_HORIZONTAL_FORCE:number = 200;
export const ARM_VERTICAL_FORCE:number = 400;
export const PLAYER_WEIGHT:number = 1;
export const SHEEP_WEIGHT:number  = 20;
export const INITIAL_FIRE_ADVANCE_RATE:number = 1;
export const FIRE_ADVANCE_RATE_INCREASE_PER_LEVEL:number = 2.0;
export const FIRE_RETREAT_SPEED:number = 80;
export const SHEEP_COLLISION_RADIUS:number = 24;
export const SHEEP_INITIAL_SPEED:number = 55;
export const SHEEP_SPIN_MAX:number = 180;
export const SHEEP_SPIN_MIN:number = -180;
export const FRIEND_LENGTH:number = 16;
export const FRIEND_COLLISION_RADIUS:number = 24;
export const FRIEND_SPIN_MAX:number = 360;
export const FRIEND_SPIN_MIN:number = -360;
export const PLAYER_START_X:number = 175;
export const PLAYER_START_Y:number = 250;
export const MUSIC_VOLUME:number = 0.25;
export const INITIAL_SHEEP_SPEED:number = 55;
export const FRICTION:number = 0.9;

type Depths = {
    FIRE_DEPTH:number,
    BUBBLE_DEPTH:number,
    MAIN_DEPTH:number,
    ARM_DEPTH:number,
    PLAYER_DEPTH:number,
    LABEL_DEPTH:number
};
export const depth: Depths = {
    BUBBLE_DEPTH: -5,
    MAIN_DEPTH: -4,
    ARM_DEPTH: -3,
    PLAYER_DEPTH: -2,
    FIRE_DEPTH: -1,
    LABEL_DEPTH: 0,
};