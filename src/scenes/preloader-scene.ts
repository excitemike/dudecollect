import * as cfg from "../settings";

export class PreloaderScene extends Phaser.Scene {
    private loadingBar: Phaser.GameObjects.Graphics;
    private progressBar: Phaser.GameObjects.Graphics;

    constructor() {
        super({key: 'PreloaderScene'})
    }

    preload(): void {
        this.cameras.main.setBackgroundColor(0x000000);
        this.createLoadingGraphics();

        // fill loading bar as we load
        this.load.on('progress', (value: number) => {
                this.progressBar.clear();
                this.progressBar.fillStyle(0xffffff, 1);
                this.progressBar.fillRect(
                    this.cameras.main.width / 4,
                    this.cameras.main.height / 2 - 16,
                    (this.cameras.main.width / 2) * value,
                    16
                );
            },
            this
        );

        // delete on complete
        this.load.on('complete', () => {
            this.progressBar.destroy();
            this.loadingBar.destroy();
        }, this);

        this.load.pack('preload', './assets/pack.json', 'preload');
    }

    create(): void {
        // create background
        const bg = this.add.image(0, 0, 'sky');
        bg.setOrigin(0,0);
        bg.setScale(this.cameras.main.width / bg.width, this.cameras.main.height / bg.height);

        // start game scenes
        this.scene.pause('PreloaderScene');
        this.scene.run('MainScene');
        this.scene.run('HudScene');
    }

    createLoadingGraphics(): void {
        this.loadingBar = this.add.graphics();
        this.loadingBar.fillStyle(0xffffff, 1);
        this.loadingBar.fillRect(
          this.cameras.main.width / 4 - 2,
          this.cameras.main.height / 2 - 18,
          this.cameras.main.width / 2 + 4,
          20
        );
        this.loadingBar.fillStyle(0x000000, 1);
        this.loadingBar.fillRect(
          this.cameras.main.width / 4 - 1,
          this.cameras.main.height / 2 - 17,
          this.cameras.main.width / 2 + 2,
          18
        );
        this.progressBar = this.add.graphics();
    }
}