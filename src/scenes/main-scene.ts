import { Player } from "../objects/player";
import { Fire } from "../objects/fire";
import { Friend } from "../objects/friend";
import { Label } from "../objects/label";
import { setMusic, toggleMusic } from "../music";
import * as Script from "../script/script";
import * as cfg from "../settings";
import { Sheep } from "../objects/sheep";

export class MainScene extends Phaser.Scene {
  private script: Script.Script;
  private scriptIndex: integer = 0;
  private toggleMusic: Phaser.Input.Keyboard.Key;
  private toggleMusicLocked: boolean;
  private player: Player;
  private platform: Phaser.GameObjects.Image;
  private levelOver: boolean;
  private fire: Fire;
  private hudLabel: Phaser.GameObjects.Text;

  constructor() {
    super({ key: "MainScene" });
    this.player = null;
    this.platform = null;
    this.levelOver = false;
    this.fire = null;
    this.hudLabel = null;
  }

  init(): void {
    setMusic("https://meyermike.com/games/dudecollect_ts/dudecollect_music.mp3");
  }

  create(): void {
    this.toggleMusic = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.M
    );
    this.toggleMusicLocked = false;

    this.registry.set("spawnAreaBottom", cfg.INITIAL_SPAWN_AREA_BOTTOM);
    this.registry.set("spawnAreaTop", cfg.INITIAL_SPAWN_AREA_TOP);
    this.registry.set("fireAdvanceRate", cfg.INITIAL_FIRE_ADVANCE_RATE);
    this.registry.set("currentLevel", 1);

    let exampleSheep:Sheep;
    let exampleFriend:Friend;
    this.script = new Script.Script(
      45,
      new Script.Loop(1, [
        new Script.Wait(2),
        new Script.RunFunc(() => {
          exampleSheep = new Sheep(this);
          exampleSheep.x = 80;
          exampleSheep.dx = 0;
          exampleSheep.y = 170;
          this.addLabel("click to grab a sheep", 160, 130, 20, 2.67);
        }),
        new Script.Wait(1),
        new Script.RunFunc(() => {
            this.addLabel("if you can reach it!", 200, 150, 20, 2.0);
        }),
        new Script.Wait(3),
        new Script.RunFunc(() => {
            // send the sheep on it's way
            exampleSheep.dx = -40;
            
            exampleFriend = new Friend(this);
            exampleFriend.x = 50;
            exampleFriend.y = 110;
            
            this.addLabel("grab friends to reach further", 175, 80, 20, 2.67);
        }),
        new Script.Wait(1),
        new Script.RunFunc(() => {
            exampleFriend.dx = -40;
            this.addLabel("get all 10 to beat level", 175, 100, 20, 2.67);
        }),
        new Script.Wait(3),
        new Script.RunFunc(() => {
          this.fire = new Fire(this);
          this.addLabel("AVOID FIRE", 175, 415, 48, 2.67);
        }),
        new Script.Wait(3),
        new Script.RunFunc(() => {
          this.restartLevel();
          this.addLabel("music:  Lost in Fractal Dimension (alt)", 20, 370, 20, 5)
            .setOrigin(0,0.5);
            this.addLabel("by:  Ultrasyd ", 20, 390, 20, 5)
            .setOrigin(0,0.5);
            this.addLabel("     ( http://ultrasyd.free.fr/ )", 20, 410, 20, 5)
            .setOrigin(0,0.5);
        }),
        new Script.RunFunc(() => {
        }),
        new Script.Loop(Number.POSITIVE_INFINITY, [new Script.RunFunc(() => {
          new Sheep(this);
        })])
      ])
    );

    this.player = new Player(this);
    this.registry.set('player', this.player);
    this.addPlatform();
    this.input.on('pointerdown', (pointer:any) => {
      if (!this.levelOver) {
        this.sheepGrabbing(pointer.x, pointer.y);
      }
    });
    this.input.on('pointerup', (pointer:any) => {
      if (!this.levelOver) {
        this.player.letGo();
      }
    });
  }

  sheepGrabbing(x:number,y:number):void{
    // pick best sheep
    const handPos = this.player.getHandPosition();
    let bestDistance = Number.POSITIVE_INFINITY;
    let sheep: Sheep = null;
    for (let i=0; i<this.children.list.length;++i) {
      const o = this.children.list[i];
      if (o instanceof Sheep) {
        const oAsSheep = o as Sheep;
        const d = Phaser.Math.Distance.Between(oAsSheep.x, oAsSheep.y, handPos.x, handPos.y);
        if (d < bestDistance) {
          bestDistance = d;
          sheep = oAsSheep;
        }
      }
    }

    if (null != sheep) {
      this.player.attemptGrabSheep(sheep);
    }
  }

  resetFriends():void{
    this.children.list.forEach((o) => {
      if (o instanceof Friend) {
        o.destroy();
      }
    });
    this.addFriends();
  }

  addFriends():void{
    for (let i=0; i<cfg.NUM_FRIENDS;++i){
      new Friend(this);
    }
  }

  updateLevelLabel():void{
    this.setHudText(`level ${this.registry.get('currentLevel')}/${cfg.LAST_LEVEL}`);
  }

  setHudText(text:string):void{
    if (null==this.hudLabel) {
      this.hudLabel = new Phaser.GameObjects.Text(this, 175, 20, '', {});
      this.hudLabel.setOrigin(0.5,0.5);
      this.hudLabel.setFontFamily('DudeCollectFont');
      this.hudLabel.setFontSize(20);
      this.hudLabel.depth = cfg.depth.LABEL_DEPTH;
      this.hudLabel.setScrollFactor(0,0);
      this.add.existing(this.hudLabel);
    }
    this.hudLabel.setText(text);
  }

  addLabel(
    text: string,
    x: number,
    y: number,
    fontSize: number,
    lifetime: number
  ): Label {
    const label = this.add.existing(
      new Label(this, x, y, text)
    );
    label.setLifeTime(lifetime);
    label.setFontFamily('DudeCollectFont');
    label.setFontSize(fontSize);
    return label;
  }

  addPlatform(): void {
    let platform = new Phaser.GameObjects.Image(
      this,
      cfg.PLATFORM_POSITION.x,
      cfg.PLATFORM_POSITION.y,
      "platform"
    );
    platform.setOrigin(0.5, 0.5);
    platform.depth = cfg.depth.MAIN_DEPTH;
    this.add.existing(platform);
    this.platform = platform;
  }

  update(_time: number, delta: number): void {
    delta *= 0.001;
    this.script.update(delta);

    // update
    this.children.list.forEach((o) => {
      if (o.active) {
        const lc = (o as unknown) as Lifecycle;
        if (undefined != lc.lifecycle_update) {
          lc.lifecycle_update(delta);
        }
      }
    });

    // collisions
    this.collisions(delta);
    // integrate
    this.children.list.forEach((o) => {
      if (o.active) {
        const lc = (o as unknown) as Lifecycle;
        if (undefined != lc.lifecycle_integrate) {
          lc.lifecycle_integrate(delta);
        }
      }
    });

    // camera movement
    if (!this.levelOver) {
      const target = -(0.5 * this.cameras.main.height - this.player.y);
      this.cameras.main.scrollY = Phaser.Math.Linear(this.cameras.main.scrollY, target, 0.0675);
    }

    // predraw
    this.children.list.forEach((o) => {
      if (o.active) {
        const lc = (o as unknown) as Lifecycle;
        if (undefined != lc.lifecycle_predraw) {
          lc.lifecycle_predraw(delta);
        }
      }
    });

    // music controls
    if (this.toggleMusic.isUp) {
      this.toggleMusicLocked = false;
    }
    if (this.toggleMusic.isDown && !this.toggleMusicLocked) {
      this.toggleMusicLocked = true;
      toggleMusic();
    }
  }

  collisions(timeDelta: number): void {
    if (null != this.player) {
      this.collisionWithPlatform(timeDelta);

      if (!this.levelOver) {
        // restart if it touches the fire
        const playerBottom = this.player.y + 0.5 * this.player.height;
        const fireTop =
          (null == this.fire) ? 10000.0 : this.fire.y - 0.5 * this.fire.displayHeight;
        if (null != this.fire && playerBottom > fireTop) {
          this.levelOver = true;
          this.player.letGo();
          this.script = this.makeLevelFailedScript();
        } else {
          // stop at sides
          const leftLimit = this.player.width / 2;
          const rightLimit = this.cameras.main.width - this.player.width / 2;
          if (this.player.x < leftLimit) {
            this.player.x = leftLimit;
            this.player.dx = 0.0;
          }
          if (this.player.x > rightLimit) {
            this.player.x = rightLimit;
            this.player.dx = 0.0;
          }

          this.friendGrabbing();
        }
      }
    }
  }

  collisionWithPlatform(timeDelta: number): void {
    // collision with platform
    if (null != this.platform) {
      const xmove = this.player.dx * timeDelta;
      const ymove = this.player.dy * timeDelta;
      const maxMove = Math.max(xmove, ymove);
      const minSide = Math.min(this.player.width, this.player.height);
      const steps = Math.ceil(maxMove / minSide);
      const platformBox = new Phaser.Geom.Rectangle(
        this.platform.x - 0.5 * this.platform.width,
        this.platform.y - 0.5 * this.platform.height,
        this.platform.width,
        this.platform.height
      );
      for (let i = 0; i <= steps; ++i) {
        const playerBox = new Phaser.Geom.Rectangle(
          (xmove * i / steps) +
            this.player.x -
            0.5 * this.player.width,
          (ymove * i / steps) +
            this.player.y -
            0.5 * this.player.height,
          this.player.width,
          this.player.height
        );
        const intersection = Phaser.Geom.Rectangle.Intersection(
          playerBox,
          platformBox
        );
        if (!intersection.isEmpty()) {
          if (
            intersection.height < intersection.width &&
            this.player.dy > 0 &&
            this.player.y < this.platform.y
          ) {
            this.player.y = intersection.y - 0.5 * this.player.height;
            this.player.dy = 0;
            this.player.dx *= cfg.FRICTION;
            break;
          }
        }
      }
    }
  }

  friendGrabbing(): void {
    this.children.list.forEach((o) => {
      if (o instanceof Friend) {
        this.player.attemptGrabFriend(o as Friend);
      }
    });

    if (this.player.getNumRescuedFriends() == cfg.NUM_FRIENDS) {
      this.levelOver = true;
      this.fire.dy = cfg.FIRE_RETREAT_SPEED;
      this.script = this.makeLevelCompleteScript();
    }
  }

  makeLevelFailedScript(): Script.Script {
    return new Script.Script(
      55,
      new Script.Loop(1, [
        new Script.RunFunc(() => {
          this.addLabel('FAILED (-o-;)', 175, this.player.y, 20, 2.67);
        }),
        new Script.Wait(2),
        new Script.RunFunc(()=>{
          this.restartLevel();
        })
      ])
    );
  }

  removeAll():void{
    this.children.list.concat().forEach((o) => {
      o.destroy();
    });
    this.player = null;
    this.platform = null;
    this.fire = null;
    this.hudLabel = null;
  }

  makeLevelCompleteScript(): Script.Script {
    return new Script.Script(
      55,
      new Script.Loop(1, [
        new Script.RunFunc(() => {
          this.addLabel('COMPLETE \\(^o^)/', 175, this.player.y, 20, 2.67);
          this.registry.set('fireAdvanceRate', this.registry.get('fireAdvanceRate') + cfg.FIRE_ADVANCE_RATE_INCREASE_PER_LEVEL);
          this.registry.set('spawnAreaTop', this.registry.get('spawnAreaTop') + cfg.SPAWN_AREA_TOP_INCREASE_PER_LEVEL);
          this.registry.set('currentLevel', this.registry.get('currentLevel') + 1);
        }),
        new Script.Wait(2),
        new Script.RunFunc(()=>{
          this.restartLevel();
        })
      ])
    );
  }

  restartLevel():void{
    this.removeAll();

    if (this.registry.get('currentLevel') > cfg.LAST_LEVEL) {
      this.addLabel("YOU WIN!", 175, 200, 72, Number.POSITIVE_INFINITY)
        .setScrollFactor(0,0);
        this.addLabel("thank you for playing!", 200, 250, 20, Number.POSITIVE_INFINITY)
          .setScrollFactor(0,0);
    } else {
      this.player = new Player(this);
      this.registry.set('player', this.player);
      this.addPlatform();
      this.fire = new Fire(this);
      this.updateLevelLabel();
      this.levelOver = false;
      this.addFriends();
      this.script = new Script.Script(55, new Script.Loop(Number.POSITIVE_INFINITY, [new Script.RunFunc(() => {
        const s = new Sheep(this);
        // bias a bit toward the player's y
        s.y = Phaser.Math.Linear(s.y, this.player.y, Math.random() * Math.random());
      })]));
      const starterSheep = new Sheep(this);
      starterSheep.dx *= 1.1;
      starterSheep.y = this.player.y - starterSheep.height;
    }
  }
}
