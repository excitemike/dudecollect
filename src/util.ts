
export function stretchBetween(object: Phaser.GameObjects.Shape | Phaser.GameObjects.Image, pointA:Phaser.Math.Vector2, pointB:Phaser.Math.Vector2, maxLen?:number):void {
    let distance:number = pointA.distance(pointB);
    if (distance == 0.0)
    {
        distance = 1.0;
    }
    
    let scale:number = 1.0;
    
    if ( (undefined != maxLen) && (distance > maxLen) )
    {
        scale = maxLen / distance;
    }
    
    object.setScale(distance * scale / object.width, 1.0);
    
    let center:Phaser.Math.Vector2 = pointA.clone().lerp(pointB, 0.5 * scale);
    object.x = center.x;
    object.y = center.y;
    
    object.setAngle(Math.atan2(pointB.y - pointA.y, pointB.x - pointA.x) * 180.0 / Math.PI);
}