import * as Phaser from 'phaser';
import { PreloaderScene } from './scenes/preloader-scene';
import { MainScene } from './scenes/main-scene';
import { HudScene } from './scenes/hud-scene';
function preload () {
    
}

function create () {
    
}

function update () {
    
}

window.addEventListener('load', () => {
    const game = new Phaser.Game({
        width: 350,
        height:450,
        type: Phaser.AUTO,
        scene: [ PreloaderScene, MainScene, HudScene ],
        title: 'Dude Collect',
        parent: 'game',
        input: { mouse: true, touch: true },
        banner: false,
        fps: { target: 30, forceSetTimeOut: true },
        disableContextMenu: true
    });
});