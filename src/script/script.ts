export class Script {
    private timeSinceTick:number;
    private ticksPerMinute:number;
    private step:Step;

    constructor(ticksPerMinute:number, step:Step) {
        this.ticksPerMinute = ticksPerMinute;
        this.timeSinceTick = this.ticksPerMinute / 60.0;
        this.step = step;
    }

    update(elapsedTime:number) {
        this.timeSinceTick += elapsedTime;
        const endTime = this.ticksPerMinute / 60.0;
        while (this.timeSinceTick > endTime) {
            this.timeSinceTick -= endTime;
            this.step.execute();
        }
    }
}

export class Step {
    /// override me!
    execute(): boolean {
        return true;
    }
    
    /// override me!
    restart(): void {

    }
}

export class Loop extends Step {
    private numLoops:number;
    private steps:Step[];
    private loopsLeft:integer;
    private prevStep:integer;
    private nextStep:integer;

    constructor(numLoops:number, steps:Step[]) {
        super();
        this.numLoops = numLoops;
        this.loopsLeft = numLoops;
        this.steps = steps.concat();
        this.nextStep = 0;
        this.prevStep = -1;
    }

    /// execute the current step
    execute(): boolean {
        // early out if nothing to run
        if (0 == this.steps.length) { return true; }

        // early out if completed
        if (this.loopsLeft <= 0) { return true; }

        let curStep = this.steps[this.nextStep];

        // if it is the first time we are running 
        // it this loop, then we need to make sure 
        // it loses any state information from the 
        // last time through
        if (this.prevStep != this.nextStep) {
            curStep.restart();
        }

        this.prevStep = this.nextStep;

        // run and maybe advance
        if (curStep.execute()) {
            this.nextStep++;
        }

        // maybe move to next time through the loop
        if (this.nextStep >= this.steps.length) {
            this.prevStep = -1;
            this.nextStep = 0;
            this.loopsLeft--;
        }

        return this.loopsLeft <= 0;
    }

    restart():void {
        this.steps.forEach(step => {
            step.restart();
        });
        this.loopsLeft = this.numLoops;
        this.prevStep = -1;
        this.nextStep = 0;
    }
}

export class RunFunc extends Step {
    private f:()=>void;

    constructor(f:()=>void) {
        super();
        this.f = f;
    }

    execute():boolean {
        this.f();
        return true;
    }
}

export class Wait extends Step {
    private ticksToWait:integer;
    private countdown:integer;

    constructor(ticksToWait:integer) {
        super();
        this.ticksToWait = ticksToWait;
        this.countdown = ticksToWait;
    }

    execute():boolean {
        this.countdown--;
        return this.countdown<=0;
    }

    restart():void{
        this.countdown = this.ticksToWait;
    }
}