import * as cfg from "./settings";

let music_src:string = null;
let playing:boolean = false
let audioElem:HTMLAudioElement = null;
let initted:boolean = false;
let interacted:boolean = false;

/// start some music looping
export function setMusic(url: string) {
    if (!initted) { init(); }
    if ((music_src != null) && (music_src != url)) {
        killMusic();
    }
    music_src = url;
    audioElem = new Audio(url);
    audioElem.volume = cfg.MUSIC_VOLUME;
    audioElem.addEventListener('playing', () => playing = true);
    audioElem.addEventListener('pause', () => playing = false);
    tryPlay();
}

export function toggleMusic() {
    playing ? audioElem.pause() : audioElem.play();
}

/// clean up whatever music is already happening
function killMusic() {
    if (audioElem) {
        audioElem.pause();
        audioElem = null;
        playing = false;
    }
}

/// react to interaction from the user (which gives permission to play music)
function interactionHandler() {
    if (!interacted) {
        document.removeEventListener('click', interactionHandler);
        document.removeEventListener('touchend', interactionHandler);
        interacted = true;
        if (music_src != null) {
            tryPlay();
        }
    }
}

/// initial setup
function init() {
    if (!initted) {
        document.addEventListener('click', interactionHandler);
        document.addEventListener('touchend', interactionHandler);
        initted = true;
    }
}

/// attempt to start the music
function tryPlay() {
    if (!playing && (audioElem != null)) {
        audioElem
            .play()
            .catch((e) => {
                audioElem.addEventListener('canplaythrough', () => {
                    tryPlay();
                });
            });
    }
}
