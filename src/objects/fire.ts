import { MovingImage } from "./movingimage";
import * as cfg from "../settings";

const fireWidth = 350;
const fireHeight = 225;
const fireInitialHeight = 440;

export class Fire extends MovingImage {

    constructor(scene: Phaser.Scene) {
        super(scene, 0, 0, 'firebg');
        this.setOrigin(0.5, 0.5);
        this.depth = cfg.depth.FIRE_DEPTH;
        scene.add.existing(this);

        this.setScale(fireWidth / this.width, fireHeight / this.height);
        this.x = this.scene.cameras.main.width / 2;
        this.y = (0.5 * fireHeight) + fireInitialHeight;
        this.dy = -this.scene.registry.get('fireAdvanceRate');
    }

    create():void {
        this.dy = -this.scene.registry.get('fireAdvanceRate');
    }
}