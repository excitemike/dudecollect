export class MovingImage extends Phaser.GameObjects.Image implements Lifecycle {
    public dx:number;
    public dy:number;
    public dr:number;

    constructor(scene: Phaser.Scene, x: number, y: number, texture: string | Phaser.Textures.Texture, frame?: string | number) {
        super(scene, x, y, texture, frame);
        this.dx = 0;
        this.dy = 0;
        this.dr = 0;
    }

    lifecycle_update(timeDelta: number): void {
        
    }

    lifecycle_integrate(timeDelta: number): void {
        this.x += this.dx * timeDelta;
        this.y += this.dy * timeDelta;
        this.angle += this.dr * timeDelta;
    }

    lifecycle_predraw(timeDelta: number): void {

    }
}