import { MovingImage } from "./movingimage";
import { Player } from "./player";
import * as cfg from "../settings";

export class Sheep extends MovingImage {
  private bubble: Phaser.GameObjects.Image = null;
  private inRangeBubble: Phaser.GameObjects.Image = null;
  private activatedBubble: Phaser.GameObjects.Image = null;
  private isGrabbed: boolean;

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, "sheep");
    this.setOrigin(0.5, 0.5);
    scene.add.existing(this);
    this.depth = cfg.depth.MAIN_DEPTH;

    this.createBubbles();
    this.setInitialPosition();
  }

  destroy(): void {
    const player = this.scene.registry.get("player") as Player;
    if (player && player.getCurrentlyGrabbedSheep() == this) {
      player.letGo();
    }
    if (this.bubble != null) {
      this.bubble.destroy();
      this.bubble = null;
    }
    if (this.inRangeBubble != null) {
      this.inRangeBubble.destroy();
      this.inRangeBubble = null;
    }
    if (this.activatedBubble != null) {
      this.activatedBubble.destroy();
      this.activatedBubble = null;
    }
    super.destroy();
  }

  createBubbles(): void {
    this.bubble = this.scene.add.image(0, 0, "bubble");
    this.bubble.setOrigin(0.5, 0.5);
    this.bubble.depth = cfg.depth.BUBBLE_DEPTH;
    this.inRangeBubble = this.scene.add.image(0, 0, "bubble_gray");
    this.inRangeBubble.setOrigin(0.5, 0.5);
    this.bubble.depth = cfg.depth.BUBBLE_DEPTH;
    this.activatedBubble = this.scene.add.image(0, 0, "bubble_activated");
    this.activatedBubble.setOrigin(0.5, 0.5);
    this.bubble.depth = cfg.depth.BUBBLE_DEPTH;
    this.isGrabbed = false;
    this.switchActiveBubble(this.bubble);
  }

  setInitialPosition(): void {
    const reg = this.scene.registry;
    const top = reg.get("spawnAreaTop");
    const bottom = reg.get("spawnAreaBottom");

    this.y = Math.random() * (top - bottom) + bottom;

    // half come in from left, half from right
    if (Math.random() < 0.5) {
      this.x = this.scene.cameras.main.width + 0.5 * this.width - 1;
      this.dx = -cfg.INITIAL_SHEEP_SPEED;
    } else {
      this.x = 1 - 0.5 * this.width;
      this.dx = cfg.INITIAL_SHEEP_SPEED;
    }

    this.randomizeSpin();

    // face random direction
    if (Math.random() < 0.5) {
      this.setFlipX(true);
    }
  }

  randomizeSpin(): void {
    // random spin.  weighted in favor of slower spins
    this.dr =
      Math.random() * (cfg.SHEEP_SPIN_MAX - cfg.SHEEP_SPIN_MIN) +
      cfg.SHEEP_SPIN_MIN;
    this.dr *= Math.random();
  }

  letGoOf(): void {
    this.randomizeSpin();
    this.isGrabbed = false;
  }
  lifecycle_update(deltaTime: number): void {
    const player = this.scene.registry.get("player") as Player;

    if (this.isGrabbed) {
      this.switchActiveBubble(this.activatedBubble);
    } else {
      if (null != player && player.canReach(this)) {
        this.switchActiveBubble(this.inRangeBubble);
      } else {
        this.switchActiveBubble(this.bubble);
      }
    }

    if (this.x < -this.width) {
      this.destroy();
    } else if (this.x > this.scene.cameras.main.width + this.width) {
      this.destroy();
    }
  }

  lifecycle_predraw(deltaTime: number): void {
    this.bubble.x = this.x;
    this.bubble.y = this.y;
    this.inRangeBubble.x = this.x;
    this.inRangeBubble.y = this.y;
    this.activatedBubble.x = this.x;
    this.activatedBubble.y = this.y;
  }

  switchActiveBubble(bubbleToSwitchTo: Phaser.GameObjects.Image): void {
    this.bubble.visible = false;
    this.inRangeBubble.visible = false;
    this.activatedBubble.visible = false;
    bubbleToSwitchTo.visible = true;
  }

  grabbed(): void {
    this.randomizeSpin();
    this.isGrabbed = true;
  }
}
