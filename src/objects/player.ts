import { MovingImage } from "./movingimage";
import { Sheep } from "./sheep";
import { Friend } from "./friend";
import { stretchBetween } from "../util";
import * as cfg from "../settings";

export class Player extends MovingImage {
  private arm: Phaser.GameObjects.Shape;
  private grabbedSheep: Sheep;
  private grabbedFriends: Friend[];
  private handPos: Phaser.Math.Vector2;

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, "player");
    this.setOrigin(0.5, 0.5);
    this.grabbedSheep = null;
    this.dx = 0;
    this.dy = 0;
    this.x = cfg.PLAYER_START_X;
    this.y = cfg.PLAYER_START_Y;
    this.grabbedFriends = [];
    this.arm = null;
    this.handPos = new Phaser.Math.Vector2(this.x, this.y);

    this.arm = new Phaser.GameObjects.Rectangle(
      scene,
      -5,
      -0.5 * cfg.ARM_THICKNESS,
      10.0,
      cfg.ARM_THICKNESS,
      0xffffff
    );
    this.arm.depth = cfg.depth.ARM_DEPTH;
    this.depth = cfg.depth.PLAYER_DEPTH;
    scene.add.existing(this.arm);
    scene.add.existing(this);
  }

  lifecycle_update(delta: number): void {
    this.sheepPhysics(delta);
    this.arm.x = this.x;
    this.arm.y = this.y;
    const accel =
      cfg.GRAVITY_FOR_JUST_PLAYER +
      cfg.GRAVITY_PER_FRIEND * this.grabbedFriends.length;
    this.dy += accel * delta;
  }

  lifecycle_predraw(delta: number): void {
    this.positionArmAndFriends();
  }

  removedFromScene(): void {
    this.arm.destroy();
    super.removedFromScene();
  }

  sheepPhysics(delta: number): void {
    if (null != this.grabbedSheep) {
      const diffx: number = this.grabbedSheep.x - this.x;
      const diffy: number = this.grabbedSheep.y - this.y;

      if (0 != diffx && 0 != diffy) {
        const dist: number = Math.sqrt(diffx * diffx + diffy * diffy);

        const directionVec: Phaser.Geom.Point = new Phaser.Geom.Point(
          diffx / dist,
          diffy / dist
        );

        // apply forces to both
        this.dx +=
          (cfg.ARM_HORIZONTAL_FORCE / cfg.PLAYER_WEIGHT) *
          delta *
          directionVec.x;
        this.grabbedSheep.dx -=
          (cfg.ARM_HORIZONTAL_FORCE / cfg.SHEEP_WEIGHT) *
          delta *
          directionVec.x;

        if (diffy < 0) {
          // never pull the player downwards
          this.dy +=
            (cfg.ARM_VERTICAL_FORCE / cfg.PLAYER_WEIGHT) *
            delta *
            directionVec.y;
          this.grabbedSheep.dy -=
            (cfg.ARM_VERTICAL_FORCE / cfg.SHEEP_WEIGHT) *
            delta *
            directionVec.y;
        }

        // if the arm is stretching too long, we need to prevent that
        const distToEdge: number = dist - cfg.SHEEP_COLLISION_RADIUS;
        if (distToEdge > this.getTotalReach()) {
          const correctionAmnt: number = distToEdge - this.getTotalReach();

          const playerScale: number =
            cfg.SHEEP_WEIGHT / (cfg.PLAYER_WEIGHT + cfg.SHEEP_WEIGHT);
          const sheepScale: number =
            cfg.PLAYER_WEIGHT / (cfg.PLAYER_WEIGHT + cfg.SHEEP_WEIGHT);

          this.x += directionVec.x * playerScale * correctionAmnt;
          this.y += directionVec.y * playerScale * correctionAmnt;
          this.grabbedSheep.x -= directionVec.x * sheepScale * correctionAmnt;
          this.grabbedSheep.y -= directionVec.y * sheepScale * correctionAmnt;
        }
      }
    }
  }

  getTotalReach(): number {
    if (0 < this.grabbedFriends.length) {
      return (
        cfg.MAX_ARM_LENGTH +
        this.grabbedFriends.length * cfg.FRIEND_LENGTH
      );
    } else {
      return cfg.MAX_ARM_LENGTH;
    }
  }

  positionArmAndFriends(): void {
    const handPos = this.handPos;
    if (null != this.grabbedSheep) {
      handPos.x = this.grabbedSheep.x - this.x;
      handPos.y = this.grabbedSheep.y - this.y;
    } else {
      this.scene.input.activePointer.updateWorldPoint(this.scene.cameras.main);
      handPos.x = this.scene.input.activePointer.worldX - this.x;
      handPos.y = this.scene.input.activePointer.worldY - this.y;
    }

    const lengthFromFriends: number = this.getTotalReach() - cfg.MAX_ARM_LENGTH;

    const targetLength = Math.sqrt(handPos.x * handPos.x + handPos.y * handPos.y);

    if (0.01 < targetLength) {
      const dist: number = Math.min(targetLength, this.getTotalReach());
      const directionVec: Phaser.Math.Vector2 = new Phaser.Math.Vector2(
        handPos.x / targetLength,
        handPos.y / targetLength
      );

      const armLength: number = (lengthFromFriends >= dist) ? 0 : dist - lengthFromFriends;

      const playerPos = new Phaser.Math.Vector2(this.x, this.y);
      const armEnd = playerPos.clone().add(directionVec.clone().scale(armLength));

      // position arm
      stretchBetween(this.arm, playerPos, armEnd);

      // position friends
      let squishedFriendLength: number = 0;
      if (0 < this.grabbedFriends.length) {
        squishedFriendLength = (dist - armLength) / this.grabbedFriends.length;
      }
      let friendOffset: Phaser.Math.Vector2 = directionVec
        .clone()
        .scale(squishedFriendLength);
      let startPoint: Phaser.Math.Vector2 = armEnd;
      let endPoint: Phaser.Math.Vector2 = startPoint.clone().add(friendOffset);
      for (let i: number = 0; i < this.grabbedFriends.length; ++i) {
        stretchBetween(this.grabbedFriends[i], startPoint, endPoint);
        startPoint = endPoint;
        endPoint = endPoint.clone().add(friendOffset);
      }
      handPos.x = endPoint.x;
      handPos.y = endPoint.y;
    }
  }

  letGo():void{
      if (null != this.grabbedSheep) {
          this.grabbedSheep.letGoOf();
          this.grabbedSheep = null;
      }
  }

  attemptGrabFriend(friend: Friend):void {
    if (!friend.hasBeenGrabbed())
    {
        if (this.canReach(friend))
        {
            // one of the tests says it's touching
            if (this.touchingFriend(friend))
            {
              friend.grab();
              this.grabbedFriends.push(friend);
            }
        }
    }
  }

  canReach(object: Phaser.GameObjects.Image):boolean{
    const diffx = object.x - this.x;
    const diffy = object.y - this.y;
    const distance = Math.sqrt( (diffx*diffx) + (diffy*diffy) ) - cfg.FRIEND_COLLISION_RADIUS;
    
    return (this.getTotalReach() >= distance);
  }

  touchingFriend(friend: Friend):boolean {
    // bodies touching
    {
      const d = Phaser.Math.Distance.Between(this.x, this.y, friend.x, friend.y);
      if (d < cfg.FRIEND_COLLISION_RADIUS + 0.5 * this.width) {
        return true;
      }
    }
    // arm reaching
    {
      const d = this.segmentPointDistance(friend.x, friend.y, this.x, this.y, this.handPos.x, this.handPos.y);
      if (d < cfg.FRIEND_COLLISION_RADIUS + 0.5 * cfg.ARM_THICKNESS) {
        return true;
      }
    }
    return false;
  }

  segmentPointDistance(px:number,py:number,sx1:number,sy1:number,sx2:number,sy2:number):number{
    const dx = sx2 - sx1;
    const dy = sy2 - sy1;
    const segLenSq = dx*dx+dy*dy;
    if (segLenSq < Phaser.Math.EPSILON) { return Phaser.Math.Distance.Between(sx1, sy1, px, py); }
    const unclampedT = ((px - sx1) * (sx2 - sx1) + (py - sy1) * (sy2 - sy1)) / segLenSq;
    const t = Phaser.Math.Clamp(unclampedT, 0, 1);
    const qx = sx1 + t * (sx2-sx1);
    const qy = sy1 + t * (sy2-sy1);
    return Phaser.Math.Distance.Between(px, py, qx, qy);
  }

  getNumRescuedFriends():number{
    return this.grabbedFriends.length;
  }

  getHandPosition():Phaser.Math.Vector2 {
    this.positionArmAndFriends();
    return this.handPos.clone();
  }

  attemptGrabSheep(sheep:Sheep):void{
    if ((this.grabbedSheep != sheep) && (null == this.grabbedSheep) && this.canReach(sheep)) {
      this.grabbedSheep = sheep;
      sheep.grabbed();
    }
  }

  getCurrentlyGrabbedSheep():Sheep{
    return this.grabbedSheep;
  }
}
