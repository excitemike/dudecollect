import { MovingImage } from "./movingimage";
import * as cfg from "../settings";

export class Friend extends MovingImage {
  private isGrabbed: boolean;
  private bubble: Phaser.GameObjects.Image = null;

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, "friend");
    this.setOrigin(0.5, 0.5);
    this.depth = cfg.depth.MAIN_DEPTH;
    scene.add.existing(this);
    this.isGrabbed = false;

    this.bubble = this.scene.add.image(0, 0, "bubble");
    this.bubble.setOrigin(0.5, 0.5);
    this.bubble.depth = cfg.depth.BUBBLE_DEPTH;

    this.setInitialPosition();
  }

  destroy():void{
    if (this.bubble != null) {
      this.bubble.destroy();
      this.bubble = null;
    }
    super.destroy();
  }

  setInitialPosition(): void {
    const reg = this.scene.registry;
    const top = reg.get("spawnAreaTop");
    const bottom = reg.get("spawnAreaBottom");
    this.x = Math.random() * this.scene.cameras.main.width;
    this.y = Math.random() * (top - bottom) + bottom;

    // random spin.  weighted in favor of slower spins
    this.dr =
      Math.random() * (cfg.FRIEND_SPIN_MAX - cfg.FRIEND_SPIN_MIN) +
      cfg.FRIEND_SPIN_MIN;
    this.dr *= Math.random();

    this.scaleX = 1;
    this.scaleY = 1;
    this.rotation = 0;
  }

  hasBeenGrabbed(): boolean {
    return this.isGrabbed;
  }

  grab(): void {
    this.isGrabbed = true;
    this.dr = 0;
    this.angle = 0;

    if (null != this.bubble) {
      this.bubble.destroy();
      this.bubble = null;
    }
  }

  lifecycle_predraw(elapsedTime: number): void {
    if (null != this.bubble) {
      this.bubble.x = this.x;
      this.bubble.y = this.y;
    }
  }
}
