import * as cfg from "../settings";

const fadeInTime = 0.25;
const fadeOutTime = 0.25;

export class Label extends Phaser.GameObjects.Text implements Lifecycle {
    private age:number;
    private lifeTime:number;
    constructor(scene: Phaser.Scene, x: number, y: number, text: string | string[]) {
        super(scene, x, y, text, {});
        this.age = 0;
        this.lifeTime = 20;
        this.setOrigin(0.5,0.5);
        this.depth = cfg.depth.LABEL_DEPTH;
        scene.add.existing(this);
    }

    setLifeTime(lifeTime:number):void{
        this.lifeTime = lifeTime;
    }

    lifecycle_update(timeDelta: number): void {
        this.age += timeDelta;
        if (this.age > this.lifeTime) {
            this.destroy();
        } else if (this.age > this.lifeTime - fadeOutTime) {
            this.alpha = Phaser.Math.Linear(1.0, 0.0, Phaser.Math.Percent(this.age, this.lifeTime - fadeOutTime, this.lifeTime));
        } else if (this.age > fadeInTime) {
            this.alpha = 1;
        } else if (this.age < fadeInTime) {
            this.alpha = Phaser.Math.Linear(0.0, 1.0, Phaser.Math.Percent(this.age, 0, fadeInTime));
        } else {
            this.alpha = 0.0;
        }
    }

    lifecycle_integrate(timeDelta: number): void {
    }

    lifecycle_predraw(timeDelta: number): void {
    }
}