interface Lifecycle {
    lifecycle_update(timeDelta: number): void;
    lifecycle_integrate(timeDelta: number):void;
    lifecycle_predraw(timeDelta: number):void;
}