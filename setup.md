# setup

1. install npm if you haven't
2. run these commands to set up build tools

    ```none
    npm install -g typescript
    npm install -g yarn
    yarn
    ```

3. run `yarn run dev` to build+test
